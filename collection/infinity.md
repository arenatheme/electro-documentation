### Configure Infinity Collection Page by Sections
1. Create a Collection page with Template product.infinity.  
2. From your Shopify admin, go to **Online Store &gt; Themes**
3. Find the theme click **Customize**
4. From the top bar drop-down menu, select **Collection pages**/ Now you will access to edit Sections for a Collection page.
    * E.g. Collection page URL https://arena-Maison.myshopify.com/collections/glasses
    * Section Configure URL: https://arena-Maison.myshopify.com/admin/themes/35024109638/editor#/collections/glasses
5. In the **preview of your store on the right side of the page**, navigate to your collection **uses the product.infinity** page template.  
6. Open the Collection infinite pages settings.
7. Click Save

### Collection Filter Options

Similar to [Default Collection Page Options](collection/collection-section.md##default-collection-page-options)