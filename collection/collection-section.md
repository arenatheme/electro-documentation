### Configure Collection Page by Sections

1. From your Shopify admin, go to **Online Store &gt; Themes**
2. Find the theme click **Customize**
3. From the top bar drop-down menu, select **Collection pages**/ Now you will access to edit Sections for a Collection page.
4. In the **preview of your store on the right side of the page**, navigate to your collection **uses the product.alternate** page template.
5. Open the Collection (alternate) pages settings.
6. Click Save

### Default Collection Page Options

Parameter | Description
- |:-:
Breadcrumb image | Image for Breakcrumb background
Section Layout Mode | Boxed: fixed layouts at Box Container <br>Wide: show entire screen 
Position of the collection description | Position to show Collection description
Enable Mode View | Show Mode View: Grid/List options to Visitor (your customer)
Enable Sort by | Show Short by options to Visitor
Products per page | How many products show per Collection page.
COLLECTION FILTER |  Options related to collection filter layout.
SIDEBAR PLACEMENT| Select your sidebar position on the page <br>Left<br>Right<br>None: Turn of the sidebar
**CONTENT**| Configure add/edit/remove/arrange block content for<br> **SIDEBAR** & **COLLECTION FILTER** &**Sub-Collection Listing**

### Collection Filter Options
Parameter | Description
- |:-
Filter position|Select position to display filter<br><li>Sidebar: Show Collection Filter in Sidebar<br><li>Body: Show Collection Filter in Collection Body Content<br><li>None: Disable Filter
Body Filter Style | **Filter position = body**<br>2 style options for Body Filter<br><li>Toggle<br><li>Drawer<br>![](/assets/body_filter.png)
Sidebar Filter Style | **Filter position = sidebar**<br>2 style options for Sidebar Filter<br><li>Full: Show all filter detail<br><li>Accordion: Only show filter label with accordion<br> ![](/assets/sidbar_filter.png)

##### CONTENT

Configure add/edit/remove/arrange block content for<br> **SIDEBAR** & **COLLECTION FILTER** &**Sub-Collection Listing**
![](/assets/sidbar_filter.png)

Parameter | Description
- |:-
Filter | Add/Configure Filter Content <br><li> Heading: Filter Heading<br><li>Disable 'AND' operator in a group filter: Only accept Filter one tag in a group filter<br><li> Filter by Group - [How to setup Group Filter Tag](https://arenathemes.freshdesk.com/support/solutions/articles/6000177980-how-to-setup-shopify-group-filter-tags-arenathemes)<br><li>Group use in filter: Manual add tag group show in the filter.<br><li>Body filter group per row: Set how many group filter show in Body Filter Toggle.
Sidebar Collection Menu| Show Collection Nestest Navigation Menu item in Sidebar. <br>[<button class = "markdown-button" name="button">Nested Menu items - Official Document</button>](https://help.shopify.com/en/manual/sell-online/online-store/menus-and-links/drop-down-menus#nesting-menu-items-to-build-drop-down-menus)
Sidebar Banner| Show Banner at sidebar
Sidebar Product| Show Product featured as sidebar
SubCollection | Catch Nested Menu item to show Sub-Collection Link in Collection Breakcrumb<br>[How to setup nestest menu to control hierarchy Collection](collection/add-sub-collection.md)<br>![](/assets/sub_collection_list.png)<br>[<button class = "markdown-button" name="button">Nested Menu items - Official Document</button>](https://help.shopify.com/en/manual/sell-online/online-store/menus-and-links/drop-down-menus#nesting-menu-items-to-build-drop-down-menus)