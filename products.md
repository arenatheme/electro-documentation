# Product pages

The product page showcases a product and its variants, and includes product images, pricing information, a product description, and an Add to cart button. Every product that you have specified in the admin to be available on your online store has its own product page on your website. There are many ways that you can customize the layout, style, and behavior of your product pages.

For more detail about product page you may visit [https://help.shopify.com/en/manual/products/understanding-products](https://help.shopify.com/en/manual/products/understanding-products)

### Creating Product

[<button class = "markdown-button" name="button">PRODUCT SHOPIFY OFFICIAL DOCUMENT</button>](https://help.shopify.com/en/manual/products)

### Adding Product Variants

[<button class = "markdown-button" name="button">ADDING VARIANT OFFICIAL DOCUMENT</button>](https://help.shopify.com/manual/products/variants)

### Product Alternate Templates
[<button class = "markdown-button" name="button"> ALTERNATE TEMPLATE SHOPIFY OFFICIAL DOCUMENT</button>](https://help.shopify.com/en/themes/customization/store/create-alternate-templates)

Maison support 6 product alternate templates.

![Product Page Templates](/assets/product_page_templates.png)

| Product Template         |Template name      | Description |  Demo |
| - |:-:| -:-|-:-|
| Product Default | product | default template | as below |
| Product Simple | product.simple | simple product template with addion function | [Demo](https://arena-Maison.myshopify.com/products/coneco-product-sample) |
| Product Simple Extend | product.simple-extended | simple product with addition section block | [Demo](https://arena-Maison.myshopify.com/products/nike-presents-vapromax) |
| Product Gallery 1 Col | product.gallery-1col | product with galery sticky image thumbnail | [Demo](https://arena-Maison.myshopify.com/products/asos-design-denim-midi-skirt) |
| Product Gallery 2 Col | product.gallery-2col | product with galery 2 Column image thumbnail | [Demo](https://arena-Maison.myshopify.com/products/asos-design-denim-midi-skirt) |
| Product Group | product.group | product with additon group function | [Demo](https://arena-Maison.myshopify.com/products/finity-product-sample) |
| Product Bundle | product.group-bundle | product suppor bundle function| [Demo](https://arena-Maison.myshopify.com/products/fixair-product-sample) |

To enable the alternate product page style for one of your products, navigate in your Shopify admin area to the Products section and start editing the product that you want to apply the alternate product page style to.

Once you’re editing your product, find the Templates settings on the right side of the product editor. Change the template from product to product.alternate.

The alternate product page has the same page settings as regular product pages. However, it may also includes a few other options, function depend on the layout.

### Configure Product Templates Layout by Section

_Shopify support only **1 product page template** to Section configure. In order to change to alternate Product templates:
In Theme Editor, find and go directly to the product you already selected Product alternate template when working with Theme Editor_

1. From your Shopify admin, go to **Online Store &gt; Themes**
2. Find the theme click **Customize**
3. From the top bar drop-down menu, select **Product pages.** Now you will access to edit Sections for the Product page.
4. In the **preview of your store on the right side of the page**, navigate to your product **that uses the product.alternate** page template.
5. Open the Product (alternate) pages settings.
6. Click Save

<!-- **Preview alternate template**

Change the Product page template view by add `?view=templatename` at the end of product URL which redirect is product template name. The default template can switch with no view name `?view=\`.

* product -> `?view=\`
* product.simple -> `?view=simple`.
* product.simple-extended -> `?view=simple-extended` -->

### Additional Functions

* Shopify Dynamic Checkout Button
* [Short Description](/products/short-description.md)
* Zoom
* Countdown Timer
* Shipping Time
* Variant Colors
* Variant Images
* Group Variant Thumbnail
* Image 360
* Video Thumbnail
* Shopify Augmented reality
* Pre-Order

Features| Section Configuration | Simple | Simple Extend | Gallery Col 1 | Gallert Col 2 | Group | Bundle
-|:-:|:-:|:-:|:-:|:-:|:-:|:-:
Exit Intent Popup Coupon|Metafield|x|x|x|x|x|x
Shopify Dynamic Checkout|Show dynamic checkout button|x|x|x|x|x|x
Short Description|Metafields Description Excerpt|x|x|x|x|x|x
Zoom|Product zoom popupt|x|x|-|-|x|x
Countdownt Timer|Metafields Countdown Timer|x|x|x|x|x|x
Shipping Time (Estimate Delivery Time) |ORDER AND SHIPPING TIME|x|x|x|x|x|x
Size Swatch|Adding Size Swatches|x|x|x|x|x|x
Color Swatch|Adding Color Swatches|x|x|x|x|x|x
Image Swatch|Use variant images for swatch color|x|x|x|x|x|x
Group Variant Thumbnail|Product image alt|x|x|x|x|x|x
Image 360|Metafield Image 360|x|x|x|x|x|x
Video Thumbnail|Product image alt|x|x|x|x|x|x
Shopify Augmented reality|Enable 3D Warehouse + App|x|x|-|-|x|x
Pre Order when Out Stock|Show Pre-order button|x|x|x|x|x|x
Size Chart Image|Metafield size chart|x|x|x|x|x|x
Image & Text Addtion Detail|Addition Block|-|x|x|x|-|-
Lookbook|Addition Block|-|x|-|-|-|-