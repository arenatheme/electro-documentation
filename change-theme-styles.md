# Change Homepage Styles

The settings\_data.json file stores the theme settings data saved from the theme editor. It can also include ‘theme styles’, also known as presets.

**Maison ** support multi- presets homepage styles that you switch by replace presets settings\_data.json.

### Steps

1. From your Shopify admin, go to **Online Store &gt; Themes.**
2. Find the theme you want to edit, and then click **Actions &gt; Edit code**
![](/assets/editcode.png)
3. On the **Edit HTML/CSS page**, locate and click on the **Config** folder to reveal its content
![](/assets/style_config.png)
4. Replace current **settings\_data.json** by the new one at **pre-setting **folder
![](/assets/style_select.png)
    * Open setting_data.json in pre-settings folder. Click to Open.
    * **Copy** content and Paste it to settings_data.json in Web Browser.
    ![](/assets/copy.png)
    * Click **Save**