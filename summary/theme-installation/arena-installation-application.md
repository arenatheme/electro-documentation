## Arena Installaton Application

This is an application for Shopify designed by our engineers to support customers easily install the theme, as well as notifications about upgrades.

We recommend to use [Chrome Browser](https://www.google.com/chrome/) when using app 

### Install Arena Installation

1. Go to [https://install.arenathemes.com](https://install.arenatheme.com/install)  
  [<button class="markdown-button">Install Arena Installation App </button>](https://install.arenatheme.com/install)
2. **Enter Your Store URL**, click **Install** and Login your Shopify

   ![](/assets/arena-installation.png)

3. Click **Install unlisted app**

   ![](/assets/install.png)

Now you can use the Arena Installation App.
---
### Install Theme Package.

1. From your Shopify admins, go to [Apps](https://www.shopify.com/admin/apps), click **Arena Installation** to access the app dashboard.

2. **Insert your purchase code**. If you don't know how to get it [please refer here](https://arenathemes.freshdesk.com/support/solutions/articles/6000116407-how-to-find-your-themeforest-item-purchase-code ):

   ![](/assets/insert purchase code.png)

3. Click **Show Installation Menu**

   ![](/assets/installation_menu.png)

4. Check **Install Theme Package** option and select **only** **Install Theme Package**.
5. Select **Theme Package**
6. Click **Install Theme** and wait to finish the installation process. **_Don't close browser during installation._**

---
### Import Demo Sample Data

**WARNING: WHEN YOU SELECT IMPORT OPTIONS IT WILL DELETE YOUR DATA & CAN'T RECOVER. Please ensure that you have backed up your data or accepted the deletion of this data.**

Before you import the data please **[BACKUP YOUR ONLINE STORE DATA](https://help.shopify.com/en/manual/sell-online/online-store/how-do-i-duplicate-my-store#using-csv-files-to-back-up-store-information)**

**To backup Product Data please follow the instruction:** [https://help.shopify.com/manual/products/import-export/export-products](https://help.shopify.com/manual/products/import-export/export-products)

You may use Shopify App: https://apps.shopify.com/backup to backup your Blog Post & Pages

1. From your Shopify admins, go to [Apps](https://www.shopify.com/admin/apps), click **Arena Installation** to access the app dashboard.

2. **Insert your purchase code**. If you don't know how to get it [please refer here](https://arenathemes.freshdesk.com/support/solutions/articles/6000116407-how-to-find-your-themeforest-item-purchase-code ):

   ![](/assets/insert purchase code.png)

3. Click **Show Installation Menu**

Import PRODUCT, BLOG & ARTICLE, COLLECTION, PAGE SAMPLE DATA will require the removal of corresponding data from your store to avoid the conflict. IT WILL DELETE DATA & CANNOT RECOVER.

   ![](/assets/installation_menu.png)

   * **Install Blos And Article**
   * **Install Collections**
   * **Install Products**
   * **Install Pages.**

![](/assets/install_options.png)

**_Don't close browser during installation._**
