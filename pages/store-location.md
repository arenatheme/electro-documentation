### Creat Store Locator Page

[<button class = "markdown-button" name="button">LINK TO DEMO PAGE</button>](https://arena-Maison.myshopify.com/pages/store-locator)

1. [Create new page](../pages.md#creat-a-page)
2. Select template "page.store.locator"
3. Save page
4. [Customize Page Content by Section](../pages.md#customize-page-by-sections)

### Configure & Add Store Location

Ensure you have an article with page.faqs template (or blog post) published.

1.  From your Shopify admin, go to **Online Store &gt; Themes** and use the Customize theme button to start customizing your theme.

2. From the top bar drop-down menu, Select **Store Locator** Page
3. Open the Page settings by Click **Store Locator Page** at Sections Sidebar.

* Change Your Setting
  * API Key
  * Geolocation service: Select this option if you would like to use GEOIP to detect **Nearby Customer Location**
  * Configure store locator: Your main store location address
  * Map style: Add json data here. You may find many Google map json style [here](http://snazzymaps.com/)
  * Add CONTENT: You may add up to 150 **Store Location** by click **Add Stores** (as Shopify limit for block Section). Each store configure include:
    * Store name
    * Store info: Store Address & Other detail
    * Operating hour
    * Store location [Type by Latitude and Longitude](http://latlong.net/)
4. Click **Save**    

