
1. From your Shopify admin, go to **Online Store > Themes**. 
2. Find the theme that you want to configure and click **Customize > Theme settings**

Parameter | Description
- |:-:
[Typography](/settings/typography.md) | Font Customization. Change out all of your store’s typography
Icon | Configure Favicon, icon on your Online Store
Colors | Configure Global Theme Style & Color Setting
General | Configure Global function on your Online Store
Fomo Popups | Configure Fomo Popups function
Shopping Cart | Configure Shopping Cart page, Shop function, Currency
Product Grid | Configure Global Setting Style to **Product Grid**
Newsletter Popup | Configure Mailchimp Popup
Checkout | Configure Checkout Page by Shopify Default

Click **Save**  to adapt your Setting
