### Configure Header Section

1. From your Shopify admin, go to **Online Store &gt; Themes**
2. Next to the Theme, click **Customize**
3. From the top bar drop-down menu, Select **Home Page**
4. Select **Footer** Section to configure
5. Click Save.

### Footer Configuration Options
Footer included 2 part; Top & Bottom.
Top Footer: included block content, you can add it manually
Bottom Footer: included Logo, Copyright detail, Social sharing icon. 

Parameter | Description
- |:-
Section Layout Mode| Boxed: fixed layouts at Box Container <br>Wide: show entire screen 
Logo Position | Logo position in Footer Botom.
Footer logo image  | Logo image
Custom logo width (in pixels) | Logo size width
Show Footer Top | Show Footer Top part
Show Footer Bottom | Show Footer Bottom part
Social icons | Show social icon
CONTENT | Social Top block content. All blocks will dislay at the same row. 

Block Content | Description
- |:-
Contact | Social Contact detail <br>Width: 1/12 - 12/12
Menu Item | Create menu item before <br> [<button class = "markdown-button" name="button">Add, remove, or edit menu items - Official Document</button>](https://help.shopify.com/en/manual/sell-online/online-store/menus-and-links/editing-menus)<br>Width: 1/12 - 12/12
Instagram | Image Gallery with Instagram images.<br>Width: 1/12 - 12/12
Newsletter | Mailchimp sign up form newsletter.<br>Width: 1/12 - 12/12

Set total block contents width options equal to 12.
Our standard grid has 12 columns. It's mean 12 columns \( grids\)  equal to the entire page width.
So a 6 grid column  would be 50% of the containing blocks width. An 8 would be 3/4th \(75%\) width.




