# Summary

## Welcome
* [Support](README.md)

## Getting started
* [Theme Installation](summary/theme-installation.md)
    * [Arena Installation App](summary/theme-installation/arena-installation-application.md)
    * [Manual Upload Theme Package](summary/theme-installation/manual-upload-theme.md)
    * [Install Requirement Apps](summary/theme-installation/app.md)
    * [Setting Currency](summary/theme-installation/currency.md)
    * [Configure Shop](summary/theme-installation/stores.md)
    * [Shopify Metafield](summary/theme-installation/shopify-metafield.md)
* [Import Demo Sample Data](summary/import-sample.md)
* [Change Theme Styles](change-theme-styles.md)
* [Theme Updated](theme-updated.md)
* [Theme Customization](theme-customization.md)

## Theme Settings
* [Theme settings](theme-settings.md)
    * [Typography](settings/typography.md)
    * [Icon](settings/icon.md)
    * [Colors](settings/styles.md)
    * [General](settings/general.md)
    * [Fomo Social Proof](settings/fomo.md)
    * [Shopping Cart](settings/cart.md)
    * [Product Grid](settings/productgrid.md)
    * [Newsletter Popup](settings/newsletter.md)

## Theme Sections
* [Sections](sections.md)

## Fixed Sections
* [Header](header.md)
* [Setup Mega Navigation](mega-navigation-setup.md)
* [Footer](footer.md)

## Page Sections
* [Create Collection](collection.md)
    * [Configure Collection Section](collection/collection-section.md)
    * [Add Sub Collection Link](collection/add-sub-collection.md)
    * [Collection Order Form](collection/order-form.md)
    * [Collection Infinity](collection/infinity.md)

## Translating themes
* [Translating theme](translate.md)
## Extensions
* [Arena Icon Font](how-to-use-and-update-arenafont.md)
* [Changelog](extensions/changelog.md)