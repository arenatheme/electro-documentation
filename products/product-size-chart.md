### Configure Size chart for multi products.

1. From your Shopify admin, go to **Online Store &gt; Themes**
2. Find the theme click **Customize**
3. From the top bar drop-down menu, select **Product pages.** Now you will access to edit Sections for the Product page.
4. In the **preview of your store on the right side of the page**, navigate to your product **that uses the product.alternate** page template.
5. Open the Product (alternate) pages settings.
6. Go to **SIZE CHART > Image size chart > Select image** to upload the size chart image file.
7. Click **Save**

### Add Custom Field to show single product page Size chart

Find more detail [How to use Custom Field Extension](/summary/theme-installation/shopify-metafield.md) to add more detail for product page. 

![](/assets/metafieldproduct.png)

* _Add Image Size Chart_

  ```
    - namespace: c_f
    - key: image_size_chart
    - value: Add image size chart url
  ```

