### Setup Navigation

[<button class = "markdown-button" name="button">Navigation - Official Document</button>](https://docs.shopify.com/manual/your-website/navigation)

### Creat a Link List in Menu

[<button class = "markdown-button" name="button">Add, remove, or edit menu items - Official Document</button>](https://help.shopify.com/en/manual/sell-online/online-store/menus-and-links/editing-menus)

You may create Link list to use:

* Main Menu
* Hamberger Menu

### Nesting menu items

[<button class = "markdown-button" name="button">Nested Menu items - Official Document</button>](https://help.shopify.com/en/manual/sell-online/online-store/menus-and-links/drop-down-menus#nesting-menu-items-to-build-drop-down-menus)

### Setup Mega Navigation

1. Create Nesting menu item to use in **Main menu**. e.g. create Main Menu nesting menu item.  
![Navigation main menu ](/assets/main_menu.png)  
2. Go to Header Section, select **Main Menu** in Main menu field.  
![](/assets/main_menu_selected.png)  
3. Click Add Content and Select type of navigation  

Parameter | Description
- |:-
Mega Menu | Horizontal Mega Menu
Mega Menu with Tabs | Horizontal Mega Menu support to show by Tab
![](/assets/mega_menu_type.png)  

#### Mega Menu

Mega Navigation will show Sub Navigation in 1 block \(1 row - up to **4 columns**\) with 12 Grid layout system. [https://getbootstrap.com/docs/4.0/layout/grid/](https://getbootstrap.com/docs/4.0/layout/grid/)

Parameter | Description
- |:-
LAYOUT | Mega navigation trigger: Add a title of the menu item(menu handle) from Main Menu you would like to show Mega Navigation<br>![](/assets/navtrigger.png)
Submenu Position | Align Sub-menu Position when hover with Screen.
Width (Sub Mega Menu Width) | Percentage compared to **Main Menu** **block width**.<br> ![](/assets/nav-mainblock.png)
Background style | Show background in Sub Mega Menu
Background image | Upload background image if Background Style Option = Image
Image size | Configure Background Image size
Background image position | Align Background image position
Min height | Set min height to Sub Mega Menu. You can adjust the height based on the content inside to cover it.
COLUMN 01 - 04| Select **Content Component** to display in each column. 

#### Content Component
Content Components | Description
- |:-
**Menu item**|[<button class = "markdown-button" name="button">Add Menu Item</button>](https://help.shopify.com/en/manual/sell-online/online-store/menus-and-links/editing-menus)<br>![](/assets/menuitem.png)
**Multiple menu items** | [<button class = "markdown-button" name="button">Add Nested Menu Item</button>](https://help.shopify.com/en/manual/sell-online/online-store/menus-and-links/drop-down-menus#nesting-menu-items-to-build-drop-down-menus)<br>![](/assets/menuitem.png)
**Product** | select product to display, we support 2 product to display at one column
**Collection**| display product in one collection as slider
**Menu item & Product**| Combine Menu item & Product
**Image**| Show one image at column.
**None**| Display white space at column

* COLUMN 01 - COLUMN 4
  * Heading: Heading title in Column.
    * Only enable the column's heading on Mobile
    * Link to
  * **Width**: It's support to select width of component column. If you select auto column width, the content of the column will be automatically calculated.The 1/12 ratio means that the width is one-twelfth of the Mega Navigation Width. Please note that The total witdh of the 4 columns component must not exceed 12.
    * Eg. Auto - Auto - Auto - Auto: It's will fix 4 columns at same width
    * Eg. 2/12 - 4/12 - 3/12 - 3/12
  * The following settings will be used for each of the above components.
    * Eg: Menu item -&gt; Use for Menu item & Multi Menu items
    * Product: -&gt; Product component
    * Collection -&gt; Collection component
    * Image -&gt; Image component
  * You can settings for total 4 columns

##### Mega Navigation with Tab

![](/assets/navi3e.png)  
Most settings will be similar to other mega navigation content types. However due to the large amount of content, so you will need to settings more.

* The theme support total **7 tabs**, each tab contain 4 **columns**. Columns Content only support 4 components:
  * Menu item
  * Multi menu item
  * Image
  * None.
* Tick **Show Tab 01** to display tab 1.
* Tab Heading: Display Tab Heading, as you can see in demo is ALLURE
