### Select Product Size Variant

![](/assets/thelook-size-variant.png)
1. From your Shopify admin, go to **Online Store &gt; Themes.**
2. Find the theme that you want to edit and click **Customize**.
3. From the top bar drop-down menu, select **Product pages.** Now you will access to edit Sections for the Product page.
4. In the **preview of your store on the right side of the page**, navigate to your product **that uses the product.alternate** page template.
5. Open the Product (alternate) pages settings.
6. Go to **COLOR AND SIZE SWATCHES**.
7. Check **Adding Size Swatches**
8. Click **Save**.