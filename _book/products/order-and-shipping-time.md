Take a look at the demo to see how it should end up functioning:  
[<button class = "markdown-button" name="button">Demo</button>](https://arena-Maison.myshopify.com/products/quisque-placerat-libero)<br>

![](/assets/delivery-date.png)

### Steps:

From your Shopify admin, go to **Online Store &gt; Themes.**

1. From your Shopify admin, go to **Online Store &gt; Themes**
2. Find the theme click **Customize**
3. From the top bar drop-down menu, select **Product pages.** Now you will access to edit Sections for the Product page.
4. In the **preview of your store on the right side of the page**, navigate to your product **that uses the product.alternate** page template.
5. Open the Product (alternate) pages settings.
6. Go to **ORDER AND SHIPPING TIME** setting
7. Click Select **Enable Order and Shipping Time**
8. Select **Deadline each day**
9. Select **Delivery time**
10. Click Save
![](/assets/delivery-option.png)