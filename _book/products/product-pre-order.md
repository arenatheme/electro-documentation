1. From your Shopify admin, go to **Online Store &gt; Themes**
2. Find the theme click **Customize**
3. From the top bar drop-down menu, select **Product pages.** Now you will access to edit Sections for the Product page.
4. In the **preview of your store on the right side of the page**, navigate to your product **that uses the product.alternate** page template.
5. Open the Product (alternate) pages settings.
6. Tick **Show Pre-order button**. It's will show Pre-order button when product out of stock.
![](/assets/pre_order_button.png)
7. Click **Save**